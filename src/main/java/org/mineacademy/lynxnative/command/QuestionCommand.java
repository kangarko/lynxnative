package org.mineacademy.lynxnative.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.ExactMatchConversationCanceller;
import org.bukkit.entity.Player;
import org.mineacademy.lynxnative.LynxNative;
import org.mineacademy.lynxnative.prompt.NamePrompt;
import org.mineacademy.lynxnative.util.Log;

/**
 * An example of creating a command that displays server-to-player conversation.
 */
public final class QuestionCommand implements CommandExecutor {

	// See SpawnEntityCommand for comments and explanations.
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission("lynx.command.question")) {
			sender.sendMessage(ChatColor.RED + "You don't have permission to use this command!");

			return true;
		}

		Player player = (Player) sender;

		// Build a new conversation
		Conversation conversation = new ConversationFactory(LynxNative.getInstance())

				// Listen for conversation ending
				.addConversationAbandonedListener(event -> {
					Conversable conversable = event.getContext().getForWhom();

					// Means the conversation has returned NULL (END_OF_CONVERSATION)
					if (event.gracefulExit()) {
						String name = (String) event.getContext().getSessionData(NamePrompt.BoardingQuestion.NAME);

						conversable.sendRawMessage(ChatColor.GOLD + "Conversation finished! Your name is: " + name);

						// Means we either called Player#abandonConversation, or typed "quit", or timed out, see below
					} else
						conversable.sendRawMessage(ChatColor.RED + "Conversation has been cancelled!");
				})

				// Listen for a specific word to cancel conversation
				.withConversationCanceller(new ExactMatchConversationCanceller("quit"))

				// Automatically cancel the conversation after 30 seconds of player not typing anything to chat
				.withTimeout(30)

				// Stop putting your answer to chat
				.withLocalEcho(false)

				// Automatically appended before all prompts (not other messages)
				.withPrefix(context -> Log.colorize("&8[&6Boarding&8] &7"))

				// Start with the first question
				.withFirstPrompt(new NamePrompt())

				.buildConversation(player);


		player.beginConversation(conversation);
		return true;
	}

}
