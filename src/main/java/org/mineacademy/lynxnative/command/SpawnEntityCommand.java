package org.mineacademy.lynxnative.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

/**
 * An example of creating a custom command, don't forget to register it in plugin.yml
 * as well as in onEnable() method in your main plugin's class!
 */
// This command supports tab-completion, hence it implements a TabCompleter class.
// We do not recommend using the all-in-one "TabExecutor" class since it requires
// the latest MC version.
public final class SpawnEntityCommand implements CommandExecutor, TabCompleter {

	// Homework:
	// Level 1 (Easy):
	// /spawne <mobType> <playerName/world> <x> <y> <z>
	// /spawne skeleton kangarko
	// /spawne skeleton world 1 5 -40
	//
	// Level 2: (Medium)
	// - make /spawne <mobType> <world> <x> <y> <z> work from the console

	/**
	 * Code below is called automatically when the given sender runs this command,
	 * the label is what you specified in plugin.yml such as /spawnentity
	 * and args[] is the message after /spawnentity separated by spaces, such as:
	 *
	 * /spawnentity creeper kangarko means args[0] is "creeper" and args[1] is "kangarko"
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		// Prevent running this command from the console.
		if (!(sender instanceof Player)) {
			sender.sendMessage("You must be a player to run this command!");

			// Returning true will finish the command and does not do anything.
			return true;
		}

		// Force at least two arguments: /spawnentity <mobType> <playerOrLocationXPoint>
		if (args.length < 2)

			// Returning false will make Bukkit send the usage message you set in plugin.yml, to the sender
			return false;

		// Cast sender to player since we have blocked non-player access above
		final Player player = (Player) sender;
		EntityType entityType;

		// Parse mob type from the first argument.
		// Using try-catch to catch invalid values and warn player.
		try {
			// Using "toUpperCase" since enums are upper cased and typing "zombie" would not otherwise work
			// so we turn it into ZOMBIE for maximum user convenience automatically.
			entityType = EntityType.valueOf(args[0].toUpperCase());

		} catch (final Throwable t) {
			sender.sendMessage(ChatColor.RED + "No such entity by name '" + args[0] + "'!");

			return true;
		}

		// Some entities cannot be spawned such as lightning bolts
		if (!entityType.isAlive() || !entityType.isSpawnable()) {
			sender.sendMessage(ChatColor.RED + "Entity " + entityType.getName() + " is not alive or spawnable!");

			return true;
		}

		// If 4 arguments are provided we assume location three points, x, y and z: /spawnentity <mobType> <x> <y> <z>
		if (args.length == 4) {
			final int x = this.parseNumber(args, 1);
			final int y = this.parseNumber(args, 2);
			final int z = this.parseNumber(args, 3);

			// Check if either of the three points was invalid
			if (x == -1 || y == -1 || z == -1) {
				sender.sendMessage(ChatColor.RED + "Please check your positions, XYZ coordinates must be a whole number! You typed: " + String.join(" ", args));

				return true;
			}

			final Location location = new Location(player.getWorld(), x, y, z);
			player.getWorld().spawnEntity(location, entityType);
			return true; // In the training I forgot this line, make sure to put it so player wont get the usage message on spawning.
		}

		// If two arguments are typed, assume mobType and the player to spawn the mob at: /spawnentity <mobType> <targetPlayer>
		else if (args.length == 2) {

			// Get player by name
			final Player targetPlayer = Bukkit.getPlayer(args[1]);

			if (targetPlayer == null) {
				sender.sendMessage(ChatColor.RED + "Player '" + args[1] + "' is not online!");

				return true;
			}

			targetPlayer.getWorld().spawnEntity(targetPlayer.getLocation(), entityType);
			return true; // In the training I forgot this line, make sure to put it so player wont get the usage message on spawning.
		}

		return false; // false = player gets to see the Usage message
	}

	// Convenience method to catch exception thrown when the number is not a number (players can type anything in ther chat)
	private int parseNumber(String[] args, int index) {
		try {
			return Integer.parseInt(args[index]);

		} catch (final Throwable t) {
			return -1;
		}
	}

	/**
	 * Called automatically when typing command and pressing TAB to help you complete its arguments.
	 */
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {

		// Only make function for the console.
		if (!(sender instanceof Player))
			return new ArrayList<>();

		final List<String> completions = new ArrayList<>();
		final Player player = (Player) sender;

		switch (args.length) {

			// If we are completing the first argument, in our case mob type, proceed
			case 1:
				for (final EntityType entityType : EntityType.values())
					if (entityType.isSpawnable() && entityType.isAlive()) {
						final String name = entityType.toString().toLowerCase();

						// Only help to complete names that the argument already starts with,
						// i.e. if I type "skel" then offer to complete to "skeleton" but not "zombie"
						if (name.startsWith(args[0]))
							completions.add(name);
					}

				// Don't forget the break otherwise EVERYTHING BELOW gets run too!
				break;

			case 2:
				for (final Player onlinePlayer : Bukkit.getOnlinePlayers()) {
					final String name = onlinePlayer.getName();

					if (name.startsWith(args[1]))
						completions.add(name);
				}

				// Also offer to complete as the X location point
				completions.add(String.valueOf(player.getLocation().getBlockX()));
				break;

			case 3:
				completions.add(String.valueOf(player.getLocation().getBlockY()));

				break;

			case 4:
				completions.add(String.valueOf(player.getLocation().getBlockZ()));

				break;
		}

		return completions;
		//return null; // null = tab complete all online player names, no tab complete = new ArrayList<>()
	}
}
