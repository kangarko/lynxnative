package org.mineacademy.lynxnative.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mineacademy.lynxnative.settings.PlayerData;

/**
 * An example of creating a command working with player data stored on disk, changing tab list name.
 */
public final class SetTabCommand implements CommandExecutor {

	// See SpawnEntityCommand for comments and explanations.
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You must be a player to execute this command!");

			return true;
		}

		if (!sender.hasPermission("lynx.command.settab")) {
			sender.sendMessage(ChatColor.RED + "You don't have permission to use this command!");

			return true;
		}

		if (args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Usage: /settab <alias>");

			return true;
		}

		String tabName = args[0];
		Player player = (Player) sender;

		player.setPlayerListName(tabName);
		PlayerData.from(player).setTabListName(tabName);

		sender.sendMessage(ChatColor.GOLD + "Your name has been updated to '" + tabName + "'.");

		return true;
	}

}
