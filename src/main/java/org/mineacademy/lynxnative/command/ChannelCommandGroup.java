package org.mineacademy.lynxnative.command;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/**
 * An example of creating a command that has different subcommands, see {@link SpawnEntityCommand} for
 * more documentation.
 *
 * Bukkit does not have an easy way for this, I recommend starting out with simply placing all
 * arguments in one class, then you can use multiple classes and reflection later.
 */
public final class ChannelCommandGroup implements CommandExecutor, TabCompleter {

	// See SpawnEntityCommand for comments and explanations.
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		// /channel
		if (args.length == 0) {
			sender.sendMessage("Welcome to the /channel command. Type /channel ? for more info.");

			return true;
		}

		final String firstArg = args[0].toLowerCase();

		if ("join".equals(firstArg)) {
			//

		} else if ("leave".equals(firstArg)) {
			//

		} else if ("?".equals(firstArg)) {
			sender.sendMessage(ChatColor.RED + "Available channel commands:");
			sender.sendMessage(ChatColor.RED + "/channel join <channel> - Join a channel.");
			sender.sendMessage(ChatColor.RED + "/channel leave <channel> - Leave a channel.");

		} else
			sender.sendMessage(ChatColor.RED + "Invalid sub-argument, type /channel ? for help.");

		return true;
	}

	// See SpawnEntityCommand for comments and examples.
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return null;
	}
}
