package org.mineacademy.lynxnative.listener;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.mineacademy.lynxnative.settings.PlayerData;

/**
 * An example of a class purely meant to listen for events (keep your code clean).
 * <p>
 * Do not forget to register events in this class, see onEnable() in your main plugin's class!
 */
// We use lombok to create a private constructor to prevent you accidentally calling "new PlayerListener",
// instead of using PlayerListener.getInstance() which is preferred if using singleton
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PlayerListener implements Listener {

	/**
	 * You can pass "new PlayerListener()" if not using this class later, but some people
	 * need to call this class throughout their plugin. In this case I recommend using a "singleton",
	 * which means creating only a singleton, which means creating only a single instance of this class
	 * as a final field and calling it anywhere.
	 */
	@Getter
	private static final PlayerListener instance = new PlayerListener();

	/**
	 * An example of listening to players connecting to your server.
	 * <p>
	 * Check out {@link AsyncPlayerPreLoginEvent} and {@link PlayerLoginEvent} for the same
	 * event which is called earlier before the actual connection is fully completed.
	 *
	 * @param event
	 */
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();

		// Use custom settings file created for each player (we create the
		// file here if it does not exist) and return the tab list name,
		// or simply the player's original name if it was not set
		String tabListName = PlayerData.from(player).getTabListName();

		// Update tab list name
		player.setPlayerListName(tabListName);
	}

	/**
	 * Example of listening to the event of player disconnecting
	 *
	 * @param event
	 */
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {

		// Save memory by removing the player data -- this won't remove data
		// from your disk, only removes the loaded instance in your plugin!
		// (Next time you call PlayerData#from, it will get loaded back again)
		PlayerData.remove(event.getPlayer());
	}
}
