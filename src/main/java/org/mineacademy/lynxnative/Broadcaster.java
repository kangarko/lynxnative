package org.mineacademy.lynxnative;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.lynxnative.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple task showing messages in the game chat
 */
public final class Broadcaster extends BukkitRunnable {

	/**
	 * The messages to show
	 */
	private final List<String> messages = new ArrayList<>();

	/**
	 * The current position signaling which message to pick, we pick messages from top to bottom
	 */
	private int index = 0;

	/**
	 * Create a new instance and put some example messages in
	 */
	public Broadcaster() {
		this.messages.add("1) Please visit our website and purchase VIP to &6continue playing&7...");
		this.messages.add("2) kangarko is a really good coder bawls");
		this.messages.add("3) Please obey the rules otherwise you shall be &cpunished&7!");
	}

	@Override
	public void run() {

		// If we have reached the last message, we need to reset
		if (this.index == this.messages.size())
			this.index = 0;

		// Send all players the same message at the given index
		for (Player player : Bukkit.getOnlinePlayers())
			Log.tell(player, "&8[&d!&8] &7" + this.messages.get(index));

		// Increase position
		this.index++;
	}
}
