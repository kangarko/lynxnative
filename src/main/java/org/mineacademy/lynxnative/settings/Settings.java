package org.mineacademy.lynxnative.settings;

import lombok.SneakyThrows;
import org.bukkit.configuration.file.YamlConfiguration;
import org.mineacademy.lynxnative.LynxNative;

import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Example configuration suited for read-only, very useful for
 * storing the main settings file which you do not need to update
 * at runtime and need to access from anywhere.
 */
public final class Settings {

	public static int PLAYER_HEALTH;
	public static String PLAYER_NAME;
	public static List<String> PLAYER_ACHIEVEMENTS;
	public static String[] MULTILINE_MESSAGE;

	// I recommend using subsections for organizational purposes.
	public final static class Boss {
		public static double DAMAGE_MULTIPLER;
		public static String ALIAS;
	}

	/**
	 * (Re)load configuration using the default settings.yml file
	 */
	public static void load() {
		LynxNative instance = LynxNative.getInstance();
		String path = "settings.yml";
		File settingsFile = new File(instance.getDataFolder(), path);

		// Use Bukkit to copy settings.yml over from your JAR to the plugin's folder
		if (!settingsFile.exists())
			instance.saveResource(path, false);

		// Load default settings for (new) keys that are not on your disk
		YamlConfiguration defaults = YamlConfiguration.loadConfiguration(new InputStreamReader(instance.getResource(path)));
		YamlConfiguration config = YamlConfiguration.loadConfiguration(settingsFile);

		// Assign to use defaults for keys not on disk but inside jar
		config.addDefaults(defaults);

		PLAYER_HEALTH = config.getInt("Player_Health");
		PLAYER_NAME = config.getString("Player_Name");
		PLAYER_ACHIEVEMENTS = config.getStringList("Player_Achievements");
		MULTILINE_MESSAGE = config.getString("Multiline_Message").split("\n");

		Boss.DAMAGE_MULTIPLER = config.getDouble("Boss.Damage_Multiplier");
		Boss.ALIAS = config.getString("Boss.Alias");


		printFields(Settings.class);
	}

	// Lazy: You can use reflection to get values of all fields -- even load them, although
	// this is not recommended since field names can be easily changed (i.e. by obfuscation),
	// leading to silent problems
	@SneakyThrows
	private static void printFields(Class<?> clazz) {
		for (Field field : clazz.getDeclaredFields())
			System.out.println(clazz.getSimpleName() + "." + field.getName() + " ---> " + field.get(null));

		// Also print fields for inner classes (such as Boss here)
		for (Class<?> superClass : clazz.getDeclaredClasses())
			printFields(superClass);
	}
}
