package org.mineacademy.lynxnative.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * A simple utility class only storing static method relating printing messages
 * to console or sending to players
 */
public final class Log {

	/**
	 * We store the console command sender class from Bukkit here for convenience.
	 */
	private static final ConsoleCommandSender console = Bukkit.getConsoleSender();

	/**
	 * Prints a message to the console automatically appending [Lynx] prefix before it,
	 * you can use short-hand color codes with & (NOT THE OTHER SYMBOL AT THE URL BELOW)
	 * here for convenience, see: https://minecraft.fandom.com/wiki/Formatting_codes#Color_codes
	 * <p>
	 * WARNING: USING THE OTHER SYMBOL AT THE LINK ABOVE may corrupt your files due to encoding issues
	 * I saw in the past.
	 *
	 * @param message
	 */
	public static void log(String message) {
		console.sendMessage("[Lynx] " + ChatColor.translateAlternateColorCodes('&', message));
	}

	/**
	 * Sends the message to the player, you can use short-hand color codes with & (NOT THE OTHER
	 * SYMBOL AT THE URL BELOW) here, see: https://minecraft.fandom.com/wiki/Formatting_codes#Color_codes
	 * <p>
	 * WARNING: USING THE OTHER SYMBOL AT THE LINK ABOVE may corrupt your files due to encoding issues
	 * I saw in the past.
	 *
	 * @param player
	 * @param message
	 */
	public static void tell(Player player, String message) {
		player.sendMessage(colorize(message));
	}
	
	public static String colorize(String message) {
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	// You can also create this "hacky" method to get your internal plugin's logger, but we prefer to use
	// console sender for color support.
	/*public static Logger getLogger() {
		return LynxNative.getPlugin(LynxNative.class).getLogger();
	}*/
}
