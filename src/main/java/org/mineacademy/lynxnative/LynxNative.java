package org.mineacademy.lynxnative;

import org.bukkit.Particle;
import org.bukkit.entity.Arrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.lynxnative.api.CowExplodeEvent;
import org.mineacademy.lynxnative.command.ChannelCommandGroup;
import org.mineacademy.lynxnative.command.FireCommand;
import org.mineacademy.lynxnative.command.QuestionCommand;
import org.mineacademy.lynxnative.command.SetTabCommand;
import org.mineacademy.lynxnative.command.SpawnEntityCommand;
import org.mineacademy.lynxnative.listener.PlayerListener;
import org.mineacademy.lynxnative.settings.Settings;

/**
 * Welcome to the example Minecraft plugin we create together in the Project Orion
 * training program from minecademy.org. This plugin relies on the native Spigot API.
 *
 * @author kangarko
 */
public final class LynxNative extends JavaPlugin implements Listener {

	/**
	 * The instance of this plugin, always refreshed and taken from the onEnable method.
	 * Basically Bukkit will create a new instance for you so that you can just assert it when
	 * your plugin is enabled/reloaded.
	 */
	private static LynxNative instance;

	/**
	 * Our broadcaster task with reload support
	 */
	private BukkitRunnable broadcasterTask;

	/**
	 * This method is called automatically each time your plugin starts or when you type /reload
	 */
	@Override
	public void onEnable() {

		// Get the instance that bukkit has made for us and assert it
		instance = this;

		// Example of logging console messages:

		// A) System.out is great for a quick debug, but not recommended for production.
		//System.out.println("LynxNative is now ready to operate!");

		// B) Bukkit.getLogger() is better but still does not allow colors or show our plugin prefix.
		//Bukkit.getLogger().info("Hey from bukkit logger!");

		// C) Bukkit also creates a logger class inside your plugin which you can use (without colors)
		//getLogger().info("Information message");
		//getLogger().warning("Warning!!");
		//getLogger().severe("lol your plugin failed");

		// D) (Recommended) Use Bukkit#getConsoleSender() to send logging messages, they also support colors.
		//Log.log(ChatColor.RED + "This message should be in red");
		//Log.log("&cThis message should be in red");

		// Example of registering events: If you want to listen to actions each player does, you need to
		// put them in a class that "implements Listener" and then register the class here:
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(PlayerListener.getInstance(), this);

		// Example of registering commands: To create a custom command using Spigot only, register it in
		// plugin.yml file, then create a new class that "implements CommandExecutor" (optionally also TabCompleter)
		// and finally, register it in your onEnable() method.
		getCommand("spawnentity").setExecutor(new SpawnEntityCommand());
		getCommand("channel").setExecutor(new ChannelCommandGroup());
		getCommand("fire").setExecutor(new FireCommand());
		getCommand("settab").setExecutor(new SetTabCommand());
		getCommand("question").setExecutor(new QuestionCommand());

		restartTasks();

		/*this.heavyCalculation(integer -> {
			// process the data here
		});*/

		// Load our custom configuration
		Settings.load();
	}

	/*public void heavyCalculation(Consumer<Integer> onDownloadFinish) {
	
		new BukkitRunnable() {
			@Override
			public void run() {
				int playerData = 1;
	
				new BukkitRunnable() {
					@Override
					public void run() {
						// do the operation right here
						onDownloadFinish.accept(playerData);
					}
				}.runTask(LynxNative.getInstance());
			}
	
			private Object connectToInternetAndDownloadData() {
					return null;
			}
		}.runTaskAsynchronously(this);
	}*/

	/**
	 * Our custom reload method, which is never used unless you manually create a command for it and call it.
	 */
	public void reload() {
		restartTasks();

		// TODO save settings here if you need to
		Settings.load();
	}

	/*
	 * Restart the message broadcaster task and stop the old one if it was created.
	 */
	private void restartTasks() {
		if (this.broadcasterTask != null)
			this.broadcasterTask.cancel();

		this.broadcasterTask = new Broadcaster();
		this.broadcasterTask.runTaskTimer(this, 0, 20);

		// I commented this out to prevent... lag :)
		//new ParticleRenderer().runTaskTimer(this, 0, 1); // 20 ticks = 1 second
	}

	// ------------------------------------------------------------------------------------------------------------
	// Events examples
	// ------------------------------------------------------------------------------------------------------------

	/**
	 * An example of listening to a chat event. You can listen to the same events multiple times,
	 * you can also specify different event priority to prevent conflicts with plugins listening
	 * to this event too.
	 * <p>
	 * Priorities mean Bukkit will run your code first if the priority is LOWEST, then it will run
	 * all plugins and their codes for the event if the priority is LOW, then NORMAL, then HIGH
	 * then HIGHEST and finally, it will use the MONITOR priority as last.
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onChatEarly(AsyncPlayerChatEvent event) {
		//System.out.println("1 Message: " + event.getMessage());

		// Prevent message from appearing in the chat or console. This will still let other plugins see the event!
		//event.setCancelled(true);
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		//System.out.println("2 Message: " + event.getMessage() + ", is cancelled? " + event.isCancelled());
	}

	/**
	 * Monitoring events is recommended for logging plugins such as if you want to "spy" which blocks were
	 * broken. Use other priorities for cancelling the event or manipulation.
	 *
	 * @param event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onChatLate(AsyncPlayerChatEvent event) {
		//System.out.println("3 Message: " + event.getMessage() + ", is cancelled? " + event.isCancelled());
	}

	/**
	 * Example listener running code when a player breaks any block.
	 *
	 * @param event
	 */
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		//System.out.println("Cancelling block placement.");

		// Prevents the block from being broken. This will send an additional packet (a message) to the client
		// that the block is still there, on slow internet connections you will see it flicker for a second.
		//event.setCancelled(true);t
	}

	/**
	 * Example project: Use BukkitRunnable to show particles
	 *
	 * @param event
	 */
	@EventHandler
	public void onArrowLaunch(ProjectileLaunchEvent event) {
		if (event.getEntity() instanceof Arrow) {
			final Arrow arrow = (Arrow) event.getEntity();

			// You can make the arrow effect only apply for players with certain permission
			//if (arrow.getShooter() instanceof Player && !((Player)arrow.getShooter()).hasPermission("lynx.feature.vip"))
			//	return;

			new BukkitRunnable() {
				@Override
				public void run() {

					// Automatically cancel the infinite runnable when arrow was removed/picked up (dead) or hit the ground
					if (arrow.isDead() || arrow.isOnGround()) {
						cancel();

						return;
					}

					arrow.getWorld().spawnParticle(Particle.HAPPY_VILLAGER, arrow.getLocation(), 1);
				}
			}.runTaskTimer(this, 0, 1);
		}
	}

	/**
	 * Example listener for clicking any mob, other players or animals (since they all implement Entity)
	 *
	 * @param event
	 */
	@EventHandler
	public void onEntityClick(PlayerInteractEntityEvent event) {

		// Since Minecraft 1.9, Bukkit calls your code TWICE, once for your right hand (main hand),
		// second for left hand (off hand). We are only interested in running the code for main hand
		// to prevent double explosions since Bukkit calls the event rapidly after each other.
		//
		// We use try-catch block to prevent code from erroring out on old Minecraft releases.
		try {
			if (event.getHand() == EquipmentSlot.OFF_HAND)
				return;
		} catch (final Throwable t) {
			// Old MC version
		}

		// Uncomment to make function, if the clicked entity is cow, we call our custom event and
		// if our event was not canceled by other developers then we make an explosion at cow's location.

		/*final Entity entity = event.getRightClicked();

		System.out.println("Entity clicked!");

		if (entity instanceof Cow) {
			final CowExplodeEvent cowEvent = new CowExplodeEvent(entity);
			Bukkit.getPluginManager().callEvent(cowEvent);

			// Only run code when event was not canceled by other plugin, and use cowEvent.getPower() which
			// can be set by anyone > this will return the last modification on the highest priority from the last
			// plugin which modified it, in this case it is only us below.
			if (!cowEvent.isCancelled())
				entity.getWorld().createExplosion(entity.getLocation(), cowEvent.getPower());
		}*/
	}

	/**
	 * Example of listening to a custom event. I will show you how to import other plugins to your source code
	 * later. This time we are listening to our own event we created and called above.
	 *
	 * @param event
	 */
	@EventHandler
	public void onCowExplode(CowExplodeEvent event) {
		System.out.println("Exploding cow, decreasing power from " + event.getPower() + " to / 2");

		event.setPower(event.getPower() / 2);
	}

	/**
	 * Returns the instance of our plugin that Bukkit created upon last (re)loading
	 *
	 * @return
	 */
	public static LynxNative getInstance() {
		return instance;
	}
}
