package org.mineacademy.lynxnative.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * An example of creating a custom event. Custom events are never called,
 * you must call them using {@link Bukkit#getPluginManager()}.callEvent() method
 *
 * Every custom event class "extends Event" and optionally "implements Cancellable" if you want
 * plugin developers able to cancel your event (and possily, game action associated with it).
 */
public final class CowExplodeEvent extends Event implements Cancellable {

	// MANDATORY: Every custom event must have the handlers list, this makes Bukkit remember
	// which plugins listen to this event (Bukkit will do the rest for you, just have this field here)
	//
	// PLEASE ALSO SEE THE BOTTOM OF THIS FILE FOR 2 MANDATORY METHODS
	private static final HandlerList handlers = new HandlerList();

	/**
	 * The entity about to explode
	 */
	private final Entity cow;

	/**
	 * The power of the explosion (20 is TNT)
	 */
	private float power = 5;

	/**
	 * Cancel the explosion? Don't forget to call your event BEFORE the explosion to make it cancelable.
	 */
	private boolean cancelled;

	/**
	 * Create a new event (this does not do anything really, you need to call this in {@link Bukkit#getPluginManager()}.callEvent())
	 * Don't forget to store this as variable before you call the event in Bukkit and then use the fields above
	 * to get modified values in case other plugins modify your explosion power, for example.
	 *
	 * @param cow
	 */
	public CowExplodeEvent(Entity cow) {
		this.cow = cow;
	}

	public Entity getCow() {
		return this.cow;
	}

	public float getPower() {
		return this.power;
	}

	public void setPower(float power) {
		this.power = power;
	}

	@Override
	public boolean isCancelled() {
		return this.cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	// MANDATORY: Every event class must have this method
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	// MANDATORY: Every event class must have this method
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
