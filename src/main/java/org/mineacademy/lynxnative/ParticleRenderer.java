package org.mineacademy.lynxnative;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public final class ParticleRenderer extends BukkitRunnable {

	@Override
	public void run() {
		final long now = System.currentTimeMillis();

		for (final Player player : Bukkit.getOnlinePlayers()) {

			new BukkitRunnable() {
				@Override
				public void run() {
					final Location center = player.getEyeLocation();
					final double radius = 2;
					final double step = 8;

					for (double degree = 0; degree <= 180; degree += step) {
						final double radians = Math.toRadians(degree);

						final double x = Math.cos(radians) * radius;
						final double z = Math.sin(radians) * radius;

						drawCircle(center, step, x, z);
					}

					System.out.println("A Showing animations took " + (System.currentTimeMillis() - now) + "ms");
				}
			}.runTaskAsynchronously(LynxNative.getInstance());
		}
	}

	/*
	 * Draws a circle around the player. Used when drawing a sphere.
	 */
	private void drawCircle(final Location center, final double step, final double height, final double radius) {
		final World world = center.getWorld();

		for (double degree = 0; degree <= 360; degree += step) {
			final double radians = Math.toRadians(degree);

			final double x = Math.cos(radians) * radius;
			final double z = Math.sin(radians) * radius;

			//System.out.println("aa");
			world.spawnParticle(Particle.DUST, center.clone().add(x, height, z), 1, new Particle.DustOptions(Color.RED, 0.17F));
		}
	}
}
